# calendar-app

calendar app is a software application designed to help users manage and organize their schedules, events, and appointments. These apps provide a digital interface for users to view, create, edit, and delete events, allowing for efficient time management.

- build using [Expo](https://expo.io), [Express.js](https://expressjs.com/), [Node.js](https://nodejs.org/en), [PostgreSql](https://www.postgresql.org/)

## Getting Started

### Installation for backend

From root directory move to backend folder usind command

```
cd backend
```

Run the command in folder name backend

```
npm install
```

Run the backend project

```
node index.js
```

### Installation for calendar app

From root directory move to calendar folder usind command

```
cd calendar
```

Run the command in folder name calender for installing all dependencies

```
npm install
```

Run the calender app

```
npm start
```
