// Sequelize setup
const { v4: uuidv4 } = require("uuid");
const { Sequelize, STRING, JSONB, UUID } = require("sequelize");

// const sequelize = new Sequelize(process.env.POSTGRES_URL);

const sequelize = new Sequelize("calender", "postgres", "admin", {
  host: "localhost",
  dialect: "postgres",
});

// Define your model
const Task = sequelize.define("tasks", {
  key: {
    type: UUID(),
    defaultValue: () => uuidv4(),
    primaryKey: true,
  },
  date: {
    type: STRING(),
  },
  todoList: {
    type: JSONB(),
    defaultValue: [],
  },
  markedDot: {
    type: JSONB(),
    defaultValue: [],
  },
});

// Sync the model with the database
sequelize.sync();

module.exports = { Task };
