const { Task } = require("../model/calendar.model");
const calendarController = {
  getAll: async (req, res) => {
    try {
      const tasks = await Task.findAll();
      return res.json(tasks);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  },
  getByDate: async (req, res) => {
    try {
      const { date } = req.body;
      const tasks = await Task.findAll({
        where: {
          date: date,
        },
      });
      return res.json(tasks);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  },

  // Create and update togather
  create: async (req, res) => {
    try {
      const { newTask } = req.body;
      const attributes = ["date", "todoList"];
      const createdTask = await Task.bulkCreate([newTask], {
        updateOnDuplicate: attributes,
      });
      res.json(createdTask);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  },

  delete: async (req, res) => {
    try {
      const key = req.params.taskId;
      const deletedTask = await Task.destroy({
        where: {
          key,
        },
      });
      if (deletedTask > 0) {
        console.log(`Task with ID ${key} has been deleted successfully.`);
      } else {
        console.log(`Task with ID ${key} not found.`);
      }
    } catch (error) {
      console.error("Error deleting task:", error);
    }
  },
};

module.exports = calendarController;
