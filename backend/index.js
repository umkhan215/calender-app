// app.js

const express = require("express");

const app = express();
require("dotenv").config();

// ADD THIS
const cors = require("cors");
app.use(cors({ origin: true, credentials: true }));

// const port = 4000;

// Body parser middleware
app.use(express.json());

// Routes

const calendarRouter = require("./routes/calendar.route");
app.use("/api/tasks", calendarRouter);

// Start the server
app.listen(process.env.PORT, () =>
  console.log("Server is running on port 4000")
);
