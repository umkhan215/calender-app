const express = require("express");
const router = express.Router();

const calendarController = require("../controllers/calendar.controller");

router.get("/", calendarController.getAll);
router.post("/tasksByDate", calendarController.getByDate);
router.post("/", calendarController.create);
router.delete("/:taskId", calendarController.delete);

module.exports = router;
