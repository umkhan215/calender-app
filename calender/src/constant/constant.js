export const badges = [
  {
    title: "Read book",
    color: "#4CD565",
  },
  {
    title: "Design",
    color: "#62CCFB",
  },
  {
    title: "Learn",
    color: "#F8D557",
  },
];

export const priorityOptions = [
  { label: "High", value: "high" },
  { label: "Medium", value: "medium" },
  { label: "Low", value: "low" },
];

export const priorityFilterOptions = [
  { label: "All", value: "all" },
  { label: "High", value: "high" },
  { label: "Medium", value: "medium" },
  { label: "Low", value: "low" },
];

export const searchOptions = [
  { label: "Title", value: "title" },
  { label: "Priority", value: "priority" },
];
