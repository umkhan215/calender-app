import axios from "axios";

const API_URL = "http://192.168.0.106:4000/api/tasks";

// Function to fetch all items
export const fetchAllItems = async () => {
  try {
    const response = await axios.post(`${API_URL}`);
    return response.data;
  } catch (error) {
    console.error("Error fetching items:", error);
  }
};

// Function to fetch all items
export const fetchItems = async (date) => {
  try {
    const response = await axios.post(`${API_URL}ByDate`, { date });
    return response.data;
  } catch (error) {
    console.error("Error fetching items:", error);
  }
};

// Function to add a new item
export const addItem = async (newTask) => {
  try {
    console.log("newTask", newTask);
    const response = await axios.post(API_URL, { newTask });
    return response.data;
  } catch (error) {
    console.error("Error adding item:", error);
  }
};

// Function to delete an item
export const deleteItem = async (key) => {
  try {
    const response = await axios.delete(`${API_URL}/${key}`);
    console.log("response.data", response.data);
    return response.data;
    // await axios.delete(`${API_URL}/${id}`);
  } catch (error) {
    console.error("Error deleting item:", error);
  }
};
