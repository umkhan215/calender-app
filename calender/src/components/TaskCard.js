import React from "react";
import { TouchableOpacity, Text, StyleSheet, View } from "react-native";
import moment from "moment";
import Badge from "./Badge";

const TaskCard = ({ item, onPress }) => {
  const badgeColor = (value) => {
    if (value === "high") return "#f43f5e";
    if (value === "medium") return "#eab308";
    if (value === "low") return "#22c55e";
    else return "#22c55e";
  };
  return (
    <TouchableOpacity onPress={onPress} style={styles.taskListContent}>
      <View
        style={{
          marginLeft: 13,
        }}
      >
        <View style={styles.row}>
          <View style={[styles.dot, { backgroundColor: item.color }]} />
          <Text style={styles.title}>{item.title}</Text>
        </View>
        <View>
          <View style={styles.noteWrapper}>
            {item.notes ? (
              <Text numberOfLines={1} style={styles.note}>
                {item.notes}
              </Text>
            ) : null}
            <Text style={styles.date}>{`${moment(item.alarm.time).format(
              "YYYY"
            )}/${moment(item.alarm.time).format("MM")}/${moment(
              item.alarm.time
            ).format("DD")}`}</Text>
          </View>
        </View>
      </View>
      <View style={[styles.verticalborder, { backgroundColor: item.color }]} />
      <Badge
        title={item.priority.label}
        style={[
          styles.badge,
          { backgroundColor: badgeColor(item.priority.value) },
        ]}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  taskListContent: {
    height: 100,
    width: "96%",
    alignSelf: "center",
    borderRadius: 4,
    shadowColor: "#2E66E7",
    backgroundColor: "#ffffff",
    marginTop: 5,
    marginBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  dot: {
    height: 12,
    width: 12,
    borderRadius: 6,
    marginRight: 8,
  },
  title: {
    color: "#554A4C",
    fontSize: 20,
    fontWeight: "700",
  },
  noteWrapper: {
    // flexDirection: "row",
    marginLeft: 20,
  },
  date: {
    color: "#BBBBBB",
    fontSize: 14,
    marginRight: 5,
  },
  note: {
    color: "#BBBBBB",
    fontSize: 14,
  },
  verticalborder: {
    height: "100%",
    width: 10,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
  },
  badge: {
    height: 23,
    width: 83,
    backgroundColor: "#4CD565",
    justifyContent: "center",
    borderRadius: 100,
    marginRight: 7,
    position: "absolute",
    right: 10,
    top: 10,
  },
});

export default TaskCard;
