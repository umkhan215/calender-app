import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Image,
} from "react-native";

const Dropdown = ({
  options,
  onSelect,
  name,
  selectedValue,
  containerStyle,
}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [selectedOption, setSelectedOption] = useState(selectedValue || null);

  const toggleDropdown = () => {
    setIsVisible(!isVisible);
  };

  const handleSelectOption = (option) => {
    setSelectedOption(option);
    onSelect(option);
    toggleDropdown();
  };

  return (
    <View style={containerStyle}>
      {name ? <Text style={styles.name}>{name}</Text> : null}
      <TouchableOpacity onPress={toggleDropdown} style={styles.container}>
        <Text style={styles.selectedOptionText}>
          {selectedOption ? selectedOption.label : "Select an option"}
        </Text>
        <Image
          source={require("../../assets/down-arrow.png")}
          style={[
            styles.image,
            isVisible && { transform: [{ rotate: "180deg" }] },
          ]}
        />
      </TouchableOpacity>

      <Modal
        transparent={true}
        visible={isVisible}
        onRequestClose={toggleDropdown}
      >
        <TouchableOpacity
          style={styles.modalOverlay}
          onPress={toggleDropdown}
        />
        <View style={styles.modalContent}>
          {options.map((option) => (
            <TouchableOpacity
              key={option.value}
              style={styles.option}
              onPress={() => handleSelectOption(option)}
            >
              <Text>{option.label}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f7f9fc",
    borderColor: "#e4e9f2",
    borderWidth: 1,
    borderRadius: 2,
    padding: 8,
    fontSize: 19,
    marginTop: 3,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  name: {
    color: "#222b45",
    fontSize: 16,
    fontWeight: "600",
    marginRight: 5,
  },
  selectedOptionText: {
    fontSize: 19,
  },
  modalOverlay: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "#fff",
  },
  option: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  image: {
    width: 14,
    height: 14,
  },
});

export default Dropdown;
