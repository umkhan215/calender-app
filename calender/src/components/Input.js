import React from "react";
import { Text, StyleSheet, TextInput, View } from "react-native";

const Input = ({ name, placeholder, value, onChangeText, style }) => {
  return (
    <View style={styles.inputWrapper}>
      <Text style={styles.name}>{name}</Text>
      <TextInput
        style={[styles.input, style]}
        onChangeText={onChangeText}
        value={value}
        placeholder={placeholder}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputWrapper: {
    marginVertical: 5,
  },
  name: {
    color: "#222b45",
    fontSize: 16,
    fontWeight: "600",
  },
  input: {
    backgroundColor: "#f7f9fc",
    borderColor: "#e4e9f2",
    borderWidth: 1,
    borderRadius: 2,
    padding: 4,
    fontSize: 19,
    marginTop: 3,
  },
});

export default Input;
