import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

const Badge = ({ title, onPress, style }) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.badge, style]}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  badge: {
    height: 23,
    width: 51,
    backgroundColor: "#F8D557",
    justifyContent: "center",
    borderRadius: 2,
  },
  text: {
    textAlign: "center",
    fontSize: 14,
    color: "#fff",
  },
});

export default Badge;
