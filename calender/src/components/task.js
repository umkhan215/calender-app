import React from 'react';
import {
  Modal,
  Platform,
  StyleSheet,
  View
} from 'react-native';

import Button from './Button';

const styles = StyleSheet.create({
  cardMain: {
    position: 'absolute',
    top: 100,
    width: 327,
    alignSelf: 'center',
    zIndex: 1000,
    elevation: 1000,
    paddingBottom: 54
  },
  card: {
    width: 327,
    borderRadius: 20,
    backgroundColor: '#ffffff',
    alignSelf: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  btnContainer: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: '#FFFFFF',
    height: 44,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: { textAlign: 'center', color:'#000', fontSize: 17, fontWeight: '500' }
});

export default class Task extends React.Component {
  render() {
    const { isModalVisible, children, setModalVisible } = this.props;
    return (
      <Modal
        animationType="fade"
        transparent
        visible={isModalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <View
          style={[
            styles.container,
            {
              ...Platform.select({
                android: {
                  // paddingTop: shouldMove ? 240 : null,
                }
              })
            }
          ]}
        >
          <View style={styles.cardMain}>
            <View style={styles.card}>{children}</View>
            <Button title={'Cancel'}  style={styles.btnContainer} textStyle={styles.textContainer} onPress={() => setModalVisible(false)}/>
          </View>
        </View>
      </Modal>
    );
  }
}
