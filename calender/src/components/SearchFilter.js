import React, { useState } from "react";
import { SearchBar } from "react-native-elements";

const SearchFilter = ({ onSearch }) => {
  const [searchText, setSearchText] = useState("");

  const handleSearch = (text) => {
    setSearchText(text);
    onSearch(text);
  };

  return (
    <SearchBar
      placeholder="Search..."
      onChangeText={handleSearch}
      value={searchText}
      lightTheme={true}
      containerStyle={{ backgroundColor: "#edf1f7", borderWidth:0 }}
      inputContainerStyle={{ backgroundColor: "#fff", height:44 }}
    />
  );
};

export default SearchFilter;
