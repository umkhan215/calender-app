import React, { Fragment, useEffect, useState } from "react";
import {
  Alert,
  Dimensions,
  Image,
  ScrollView,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { CalendarList } from "react-native-calendars";
import moment from "moment";
import * as Calendar from "expo-calendar";
import * as Localization from "expo-localization";
import DateTimePicker from "react-native-modal-datetime-picker";
import { v4 as uuidv4 } from "uuid";
import { useKeyboardHeight } from "@calendar/hooks";
import { useStore } from "@calendar/store";
import { Routes } from "@calendar/navigation";
import { SafeAreaView } from "react-native-safe-area-context";
import Button from "../../components/Button";
import Badge from "../../components/Badge";
import Input from "../../components/Input";
import { badges, priorityOptions } from "../../constant/constant";
import { styles } from "./styles";
import Dropdown from "../../components/Dropdown";

const { width: vw } = Dimensions.get("window");

export default function CreateTask({ navigation, route }) {
  const { updateTodo } = useStore((state) => ({
    updateTodo: state.updateTodo,
  }));

  const keyboardHeight = useKeyboardHeight();

  const createNewCalendar = route.params?.createNewCalendar ?? (() => null);
  const updateCurrentTask = route.params?.updateCurrentTask ?? (() => null);
  const currentDate = route.params?.currentDate ?? (() => null);

  const [selectedDay, setSelectedDay] = useState({
    [`${moment().format("YYYY")}-${moment().format("MM")}-${moment().format(
      "DD"
    )}`]: {
      selected: true,
      selectedColor: "#2E66E7",
    },
  });
  const [currentDay, setCurrentDay] = useState(moment().format());
  const [taskResponse, setTaskResponse] = useState([]);
  const [taskText, setTaskText] = useState("");
  const [notesText, setNotesText] = useState("");
  const [priorityText, setPriorityText] = useState(priorityOptions[0]);
  const [visibleHeight, setVisibleHeight] = useState(
    Dimensions.get("window").height
  );
  const [isAlarmSet, setAlarmSet] = useState(false);
  const [alarmTime, setAlarmTime] = useState(moment().format());
  const [isDateTimePickerVisible, setDateTimePickerVisible] = useState(false);

  const handleSelect = (option) => {
    setPriorityText(option);
  };

  useEffect(() => {
    if (keyboardHeight > 0) {
      setVisibleHeight(Dimensions.get("window").height - keyboardHeight);
    } else if (keyboardHeight === 0) {
      setVisibleHeight(Dimensions.get("window").height);
    }
  }, [keyboardHeight]);

  const handleAlarmSet = () => {
    setAlarmSet(!isAlarmSet);
  };

  const synchronizeCalendar = async () => {
    const calendarId = await createNewCalendar();
    try {
      const createEventId = await addEventsToCalendar(calendarId);
      handleCreateEventData(createEventId);
    } catch (e) {
      Alert.alert(e.message);
    }
  };

  const addEventsToCalendar = async (calendarId) => {
    const event = {
      title: taskText,
      notes: notesText,
      startDate: moment(alarmTime).add(0, "m").toDate(),
      endDate: moment(alarmTime).add(5, "m").toDate(),
      timeZone: Localization.timezone,
    };

    try {
      const createEventAsyncResNew = await Calendar.createEventAsync(
        calendarId.toString(),
        event
      );
      return createEventAsyncResNew;
    } catch (error) {
      console.log(error);
    }
  };

  const showDateTimePicker = () => setDateTimePickerVisible(true);

  const hideDateTimePicker = () => setDateTimePickerVisible(false);

  const handleSuggestion = (vale) => setTaskText(vale);

  const getUpdatedTodoList = () => {
    const todo = [
      {
        key: 1,
        title: taskText,
        notes: notesText,
        priority: priorityText || priorityOptions[0],
        alarm: {
          time: alarmTime || moment().format(),
          isOn: isAlarmSet || false,
          createEventAsyncRes: createEventId,
        },
        color: `rgb(${Math.floor(Math.random() * Math.floor(256))},${Math.floor(
          Math.random() * Math.floor(256)
        )},${Math.floor(Math.random() * Math.floor(256))})`,
      },
    ];
    const newList = taskResponse[0].todoList;
    return [...newList, ...todo];
  };

  const calendarDeleteFormatter = (task) => {
    return {
      key: task.key,
      date: task.date,
      todoList: getUpdatedTodoList(),
      markedDot: task.markedDot,
    };
  };

  const handleCreateEventData = async (createEventId) => {
    const creatTodo = {
      key: uuidv4(),
      date: `${moment(currentDay).format("YYYY")}-${moment(currentDay).format(
        "MM"
      )}-${moment(currentDay).format("DD")}`,
      todoList: [
        {
          key: uuidv4(),
          title: taskText,
          notes: notesText,
          priority: priorityText || priorityOptions[0],
          alarm: {
            time: alarmTime || moment().format(),
            isOn: isAlarmSet || false,
            createEventAsyncRes: createEventId,
          },
          color: `rgb(${Math.floor(
            Math.random() * Math.floor(256)
          )},${Math.floor(Math.random() * Math.floor(256))},${Math.floor(
            Math.random() * Math.floor(256)
          )})`,
        },
      ],
      markedDot: {
        date: currentDay,
        dots: [
          {
            key: uuidv4(),
            color: "#2E66E7",
            selectedDotColor: "#2E66E7",
          },
        ],
      },
    };
    navigation.navigate(Routes.HOME);
    await updateTodo(creatTodo);
    updateCurrentTask(currentDate);
  };

  const handleDatePicked = (date) => {
    const selectedDatePicked = currentDay;
    const hour = moment(date).hour();
    const minute = moment(date).minute();
    const newModifiedDay = moment(selectedDatePicked).hour(hour).minute(minute);
    setAlarmTime(newModifiedDay);
    hideDateTimePicker();
  };

  return (
    <Fragment>
      <DateTimePicker
        isVisible={isDateTimePickerVisible}
        onConfirm={handleDatePicked}
        onCancel={hideDateTimePicker}
        mode="time"
        date={new Date()}
        isDarkModeEnabled
      />

      <SafeAreaView style={styles.container}>
        <View
          style={{
            height: visibleHeight,
          }}
        >
          <ScrollView
            contentContainerStyle={{
              paddingBottom: 100,
            }}
          >
            <View style={styles.backButton}>
              <TouchableOpacity
                onPress={() => navigation.navigate(Routes.HOME)}
                style={{ marginRight: vw / 2 - 120, marginLeft: 20 }}
              >
                <Image
                  style={{ height: 25, width: 40 }}
                  source={require("../../../assets/back.png")}
                  resizeMode="contain"
                />
              </TouchableOpacity>

              <Text style={styles.newTask}>New Task</Text>
            </View>
            <View style={styles.calenderContainer}>
              <CalendarList
                style={{
                  width: 350,
                  height: 350,
                }}
                current={currentDay}
                minDate={moment().format()}
                horizontal
                pastScrollRange={0}
                pagingEnabled
                calendarWidth={350}
                onDayPress={(day) => {
                  setSelectedDay({
                    [day.dateString]: {
                      selected: true,
                      selectedColor: "#2E66E7",
                    },
                  });
                  setCurrentDay(day.dateString);
                  setAlarmTime(day.dateString);
                }}
                monthFormat="yyyy MMMM"
                hideArrows
                markingType="custom"
                markedDates={selectedDay}
              />
            </View>
            <View style={styles.taskContainer}>
              <Input
                name={"Title"}
                placeholder="What do you need to do?"
                value={taskText}
                onChangeText={setTaskText}
              />
              <Text style={styles.suggestion}>Suggestion</Text>
              <View style={{ flexDirection: "row", marginTop: 5 }}>
                {badges.map((item) => (
                  <Badge
                    title={item.title}
                    onPress={() => handleSuggestion(item.title)}
                    style={[styles.badge, { backgroundColor: item.color }]}
                  />
                ))}
              </View>
              <View style={styles.notesContent} />
              <Input
                name={"Description"}
                placeholder="Enter notes about the task."
                value={notesText}
                onChangeText={setNotesText}
              />
              <View style={styles.notesContent} />
              <Dropdown
                name={"Priority"}
                options={priorityOptions}
                selectedValue={priorityOptions[0]}
                onSelect={handleSelect}
              />
              <View style={styles.separator} />
              <View>
                <Text style={styles.lable}>Times</Text>
                <TouchableOpacity
                  onPress={() => showDateTimePicker()}
                  style={styles.input}
                >
                  <Text style={{ fontSize: 19 }}>
                    {moment(alarmTime).format("h:mm A")}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.separator} />
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <View style={{ width: "100%" }}>
                  <Text style={styles.lable}>Alarm</Text>
                  <View style={styles.alarm}>
                    <Text style={{ fontSize: 19 }}>
                      {moment(alarmTime).format("h:mm A")}
                    </Text>
                    <Switch value={isAlarmSet} onValueChange={handleAlarmSet} />
                  </View>
                </View>
              </View>
            </View>
            <Button
              title={"ADD YOUR TASK"}
              style={[
                styles.createTaskButton,
                {
                  backgroundColor:
                    taskText === "" ? "rgba(46, 102, 231,0.5)" : "#2E66E7",
                },
              ]}
              disabled={taskText === ""}
              onPress={async () => {
                if (isAlarmSet) {
                  await synchronizeCalendar();
                }
                if (!isAlarmSet) {
                  handleCreateEventData();
                }
              }}
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    </Fragment>
  );
}
