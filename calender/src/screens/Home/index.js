import React, { Fragment, useEffect, useState } from "react";
import {
  Alert,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import moment from "moment";
import * as Calendar from "expo-calendar";
import * as Localization from "expo-localization";

import CalendarStrip from "react-native-calendar-strip";
import DateTimePicker from "react-native-modal-datetime-picker";
import { Task } from "@calendar/components";
import { useStore } from "@calendar/store";
import { SafeAreaView } from "react-native-safe-area-context";
import SearchFilter from "../../components/SearchFilter";
import Button from "../../components/Button";
import Badge from "../../components/Badge";
import TaskCard from "../../components/TaskCard";
import { styles } from "./styles";
import Input from "../../components/Input";
import {
  badges,
  priorityFilterOptions,
  priorityOptions,
  searchOptions,
} from "../../constant/constant";
import Dropdown from "../../components/Dropdown";
import { addItem, deleteItem } from "../../services/service";

const datesWhitelist = [
  {
    start: moment(),
    end: moment().add(365, "days"), // total 4 days enabled
  },
];

export default function Home({ navigation }) {
  const { updateTodo, updateSelectedTask, deleteSelectedTask, todo } = useStore(
    (state) => ({
      updateSelectedTask: state.updateSelectedTask,
      deleteSelectedTask: state.deleteSelectedTask,
      updateTodo: state.updateTodo,
      todo: state.todo,
    })
  );

  const [todoList, setTodoList] = useState([]);
  const [markedDate, setMarkedDate] = useState([]);
  const [currentDate, setCurrentDate] = useState(
    `${moment().format("YYYY")}-${moment().format("MM")}-${moment().format(
      "DD"
    )}`
  );
  const [isModalVisible, setModalVisible] = useState(false);
  const [selectedTask, setSelectedTask] = useState(null);
  const [taskResponse, setTaskResponse] = useState([]);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [filteredTodoList, setFilteredTodoList] = useState(todoList);
  const [isDateTimePickerVisible, setDateTimePickerVisible] = useState(false);

  const handleSearch = (searchText) => {
    const filtered = todoList.filter((todo) =>
      todo.title.toLowerCase().includes(searchText.toLowerCase())
    );
    setFilteredTodoList(filtered);

    if (!searchText.length) setFilteredTodoList(todoList);
  };

  const handleSearchPriority = (searchText) => {
    const filtered = todoList.filter(
      (todo) => todo.priority.value === searchText.value
    );
    setFilteredTodoList(filtered);

    if (searchText.value === "all") setFilteredTodoList(todoList);
  };

  useEffect(() => {
    handleDeletePreviousDayTask(todo);
  }, [todo, currentDate]);

  useEffect(() => {
    if (todo?.length) {
      const selectedTodoByDate = todo?.filter(
        (item) => item?.date === currentDate
      );
      if (selectedTodoByDate?.length) addTask(selectedTodoByDate[0]);
    }
  }, [todo, currentDate]);

  useEffect(() => {
    setFilteredTodoList(todoList);
  }, [todoList]);

  useEffect(() => {}, [todoList, todo]);

  const handleDeletePreviousDayTask = async (oldTodo) => {
    try {
      if (oldTodo.length) {
        const todayDate = `${moment().format("YYYY")}-${moment().format(
          "MM"
        )}-${moment().format("DD")}`;
        const checkDate = moment(todayDate);
        await oldTodo.filter((item) => {
          const currDate = moment(item.date);
          const checkedDate = checkDate.diff(currDate, "days");
          if (checkedDate > 0) {
            item.todoList.forEach(async (listValue) => {
              try {
                await Calendar.deleteEventAsync(
                  listValue.alarm.createEventAsyncRes.toString()
                );
              } catch (error) {
                console.log(error);
              }
            });
            return false;
          }
          return true;
        });

        // await AsyncStorage.setItem('TODO', JSON.stringify(updatedList));
        updateCurrentTask(currentDate);
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  const handleModalVisible = () => {
    setModalVisible(!isModalVisible);
  };

  const updateCurrentTask = async (currentDate) => {
    try {
      if (todo) {
        const markDot = todo.map((item) => item.markedDot);
        const todoLists = todo.filter((item) => {
          if (currentDate === item.date) {
            return true;
          }
          return false;
        });
        setMarkedDate(markDot);
        if (todoLists.length) {
          setTodoList(todoLists[0].todoList);
        } else {
          setTodoList([]);
        }
      }
    } catch (error) {
      console.log("updateCurrentTask", error.message);
    }
  };

  const deleteCurrentTask = async (currentDate) => {
    try {
      if (todo) {
        const markDot = todo.map((item) => item.markedDot);
        const todoLists = todo.filter((item) => {
          if (currentDate === item.date) {
            return true;
          }
          return false;
        });
        setMarkedDate(markDot);
        if (todoLists.length > 1) {
          setTodoList(todoLists[0].todoList);
        } else {
          setTodoList([]);
        }
      }
    } catch (error) {
      console.log("updateCurrentTask", error.message);
    }
  };

  const showDateTimePicker = () => setDateTimePickerVisible(true);

  const hideDateTimePicker = () => setDateTimePickerVisible(false);

  const handleDatePicked = (date) => {
    let prevSelectedTask = JSON.parse(JSON.stringify(selectedTask));
    const selectedDatePicked = prevSelectedTask.alarm.time;
    const hour = moment(date).hour();
    const minute = moment(date).minute();
    let newModifiedDay = moment(selectedDatePicked).hour(hour).minute(minute);
    prevSelectedTask.alarm.time = newModifiedDay;
    setSelectedTask(prevSelectedTask);
    hideDateTimePicker();
  };

  const handleAlarmSet = () => {
    let prevSelectedTask = JSON.parse(JSON.stringify(selectedTask));
    prevSelectedTask.alarm.isOn = !prevSelectedTask.alarm.isOn;
    setSelectedTask(prevSelectedTask);
  };

  const updateAlarm = async () => {
    const calendarId = await createNewCalendar();
    const event = {
      title: selectedTask.title,
      notes: selectedTask.notes,
      startDate: moment(selectedTask?.alarm.time).add(0, "m").toDate(),
      endDate: moment(selectedTask?.alarm.time).add(5, "m").toDate(),
      timeZone: Localization.timezone,
    };

    if (!selectedTask?.alarm.createEventAsyncRes) {
      try {
        const createEventAsyncRes = await Calendar.createEventAsync(
          calendarId.toString(),
          event
        );
        let updateTask = JSON.parse(JSON.stringify(selectedTask));
        updateTask.alarm.createEventAsyncRes = createEventAsyncRes;
        setSelectedTask(updateTask);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        await Calendar.updateEventAsync(
          selectedTask?.alarm.createEventAsyncRes.toString(),
          event
        );
      } catch (error) {
        console.log(error);
      }
    }
  };

  const deleteAlarm = async () => {
    try {
      if (selectedTask?.alarm.createEventAsyncRes) {
        await Calendar.deleteEventAsync(
          selectedTask?.alarm.createEventAsyncRes
        );
      }
      let updateTask = JSON.parse(JSON.stringify(selectedTask));
      updateTask.alarm.createEventAsyncRes = "";
      setSelectedTask(updateTask);
    } catch (error) {
      console.log("deleteAlarm", error.message);
    }
  };

  const getEvent = async () => {
    if (selectedTask?.alarm.createEventAsyncRes) {
      try {
        await Calendar.getEventAsync(
          selectedTask?.alarm.createEventAsyncRes.toString()
        );
      } catch (error) {
        console.log(error);
      }
    }
  };

  const createNewCalendar = async () => {
    const defaultCalendarSource =
      Platform.OS === "ios"
        ? await Calendar.getDefaultCalendarAsync(Calendar.EntityTypes.EVENT)
        : { isLocalAccount: true, name: "Google Calendar" };

    const newCalendar = {
      title: "Personal",
      entityType: Calendar.EntityTypes.EVENT,
      color: "#2196F3",
      sourceId: defaultCalendarSource?.sourceId || undefined,
      source: defaultCalendarSource,
      name: "internal",
      accessLevel: Calendar.CalendarAccessLevel.OWNER,
      ownerAccount: "personal",
    };

    let calendarId = null;

    try {
      calendarId = await Calendar.createCalendarAsync(newCalendar);
    } catch (e) {
      Alert.alert(e.message);
    }

    return calendarId;
  };

  const handleSuggestion = (value) => {
    let prevSelectedTask = JSON.parse(JSON.stringify(selectedTask));
    prevSelectedTask.title = value;
    setSelectedTask(prevSelectedTask);
  };

  const getUpdatedTodoList = (key, updatedList) => {
    const newList = taskResponse[0].todoList?.filter(
      (item) => item.key !== key
    );
    return [...newList, updatedList];
  };

  const getDeletedTodoList = (key) => {
    const newList = taskResponse[0].todoList?.filter(
      (item) => item.key !== key
    );
    return newList;
  };

  const calendarFormatter = (task) => {
    return {
      key: task.key,
      date: task.date,
      todoList: getUpdatedTodoList(selectedTask.key, selectedTask),
      markedDot: task.markedDot,
    };
  };

  const calendarDeleteFormatter = (task) => {
    return {
      key: task.key,
      date: task.date,
      todoList: getDeletedTodoList(selectedTask.key),
      markedDot: task.markedDot,
    };
  };

  const addTask = async (task) => {
    const response = await addItem(task);
    return response;
  };

  const handleUpdate = async () => {
    handleModalVisible();
    if (selectedTask?.alarm.isOn) {
      await updateAlarm();
    } else {
      await deleteAlarm();
    }
    await updateSelectedTask({
      date: currentDate,
      todo: selectedTask,
    });
    updateCurrentTask(currentDate);
  };

  const handleDelete = async () => {
    handleModalVisible();
    deleteAlarm();
    await deleteSelectedTask({
      date: currentDate,
      todo: selectedTask,
    });
    deleteCurrentTask(currentDate);
    if (todo?.length) {
      const selectedTodoByDate = todo?.filter(
        (item) => item?.date === currentDate
      );
      if (
        selectedTodoByDate?.length &&
        selectedTodoByDate[0].todoList.length === 1
      )
        await deleteItem(selectedTodoByDate[0]?.key);
    }
    // await updateItem(currentDate, getDeletedTodoList(selectedTask.key));
    // if (taskResponse[0].todoList.length === 1)
    //   return await deleteItem(taskResponse[0]?.key);
    // else return await addTask(calendarDeleteFormatter(taskResponse[0]));
  };

  return (
    <Fragment>
      {selectedTask !== null && (
        <Task {...{ setModalVisible, isModalVisible }}>
          <DateTimePicker
            isVisible={isDateTimePickerVisible}
            onConfirm={handleDatePicked}
            onCancel={hideDateTimePicker}
            mode="time"
            date={new Date()}
            isDarkModeEnabled
          />
          <View style={styles.taskContainer}>
            <Input
              name={"Title"}
              placeholder="What do you need to do?"
              value={selectedTask.title}
              onChangeText={(text) => {
                let prevSelectedTask = JSON.parse(JSON.stringify(selectedTask));
                prevSelectedTask.title = text;
                setSelectedTask(prevSelectedTask);
              }}
            />
            <Text style={styles.suggestion}>Suggestion</Text>
            <View style={{ flexDirection: "row", marginTop: 5 }}>
              {badges.map((item) => (
                <Badge
                  title={item.title}
                  onPress={() => handleSuggestion(item.title)}
                  style={[styles.badge, { backgroundColor: item.color }]}
                />
              ))}
            </View>
            <View style={styles.notesContent} />
            <View>
              <Input
                name={"Description"}
                placeholder="Enter notes about the task."
                value={selectedTask.notes}
                onChangeText={(text) => {
                  let prevSelectedTask = JSON.parse(
                    JSON.stringify(selectedTask)
                  );
                  prevSelectedTask.notes = text;
                  setSelectedTask(prevSelectedTask);
                }}
              />
            </View>
            <View style={styles.notesContent} />
            <Dropdown
              name={"Priority"}
              options={priorityOptions}
              selectedValue={selectedTask.priority}
              onSelect={(option) => {
                let prevSelectedTask = JSON.parse(JSON.stringify(selectedTask));
                prevSelectedTask.priority = option;
                setSelectedTask(prevSelectedTask);
              }}
            />
            <View style={styles.separator} />
            <View>
              <Text style={styles.lable}>Times</Text>
              <TouchableOpacity
                onPress={() => showDateTimePicker()}
                style={styles.input}
              >
                <Text style={{ fontSize: 19 }}>
                  {moment(selectedTask?.alarm?.time || moment()).format(
                    "h:mm A"
                  )}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.separator} />
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <View style={{ width: "100%" }}>
                <Text style={styles.lable}>Alarm</Text>
                <View style={styles.alarm}>
                  <Text style={{ fontSize: 19 }}>
                    {moment(selectedTask?.alarm?.time || moment()).format(
                      "h:mm A"
                    )}
                  </Text>
                  <Switch
                    value={selectedTask?.alarm?.isOn || false}
                    onValueChange={handleAlarmSet}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Button
                title={"UPDATE"}
                style={[
                  styles.updateButton,
                  {
                    backgroundColor:
                      selectedTask.title === ""
                        ? "rgba(46, 102, 231,0.5)"
                        : "#2E66E7",
                  },
                ]}
                onPress={handleUpdate}
                disabled={selectedTask.title === ""}
              />
              <Button
                title={"DELETE"}
                style={styles.deleteButton}
                onPress={handleDelete}
              />
            </View>
          </View>
        </Task>
      )}
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#edf1f7",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            margin: 8,
          }}
        >
          <View style={{ width: "82%" }}>
            <Dropdown
              name={"Filter By"}
              options={searchOptions}
              selectedValue={searchOptions[0]}
              onSelect={(option) => {
                setFilterBy(option);
                setFilteredTodoList(todoList);
              }}
              containerStyle={styles.filter}
            />
          </View>
        </View>
        <View style={{ width: "100%" }}>
          {filterBy === searchOptions[0] ? (
            <SearchFilter onSearch={handleSearch} />
          ) : (
            <View style={{ padding: 8 }}>
              <Dropdown
                options={priorityFilterOptions}
                selectedValue={priorityFilterOptions[0]}
                onSelect={(option) => {
                  handleSearchPriority(option);
                }}
              />
            </View>
          )}
        </View>
        <CalendarStrip
          calendarAnimation={{ type: "sequence", duration: 30 }}
          daySelectionAnimation={{
            type: "background",
            duration: 200,
          }}
          style={{
            height: 150,
            paddingTop: 20,
            paddingBottom: 20,
            marginHorizontal: 8,
            backgroundColor: "#fff",
            borderRadius: 4,
          }}
          calendarHeaderStyle={{ color: "#000000" }}
          dateNumberStyle={{ color: "#000000", paddingTop: 10 }}
          dateNameStyle={{ color: "#BBBBBB" }}
          highlightDateNumberStyle={{
            color: "#fff",
            backgroundColor: "#2E66E7",
            marginTop: 10,
            height: 35,
            width: 35,
            textAlign: "center",
            borderRadius: 17.5,
            overflow: "hidden",
            paddingTop: 6,
            fontWeight: "400",
            justifyContent: "center",
            alignItems: "center",
          }}
          highlightDateNameStyle={{ color: "#2E66E7" }}
          disabledDateNameStyle={{ color: "grey" }}
          disabledDateNumberStyle={{ color: "grey", paddingTop: 10 }}
          datesWhitelist={datesWhitelist}
          iconLeft={require("../../../assets/left-arrow.png")}
          iconRight={require("../../../assets/right-arrow.png")}
          iconContainer={{ flex: 0.1 }}
          // If you get this error => undefined is not an object (evaluating 'datesList[_this.state.numVisibleDays - 1].date')
          // temp: https://github.com/BugiDev/react-native-calendar-strip/issues/303#issuecomment-864510769
          markedDates={markedDate}
          selectedDate={currentDate}
          onDateSelected={(date) => {
            const selectedDate = `${moment(date).format("YYYY")}-${moment(
              date
            ).format("MM")}-${moment(date).format("DD")}`;
            updateCurrentTask(selectedDate);
            setCurrentDate(selectedDate);
          }}
        />
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("CreateTask", {
              updateCurrentTask: updateCurrentTask,
              currentDate,
              createNewCalendar: createNewCalendar,
            })
          }
          style={styles.viewTask}
        >
          <Image
            source={require("../../../assets/plus.png")}
            style={{
              height: 30,
              width: 30,
            }}
          />
        </TouchableOpacity>
        <View
          style={{
            width: "100%",
            height: Dimensions.get("window").height - 170,
          }}
        >
          <ScrollView
            contentContainerStyle={{
              paddingBottom: 140,
            }}
          >
            {filteredTodoList.map((item) => (
              <TaskCard
                key={item.key}
                item={item}
                onPress={() => {
                  setSelectedTask(item);
                  setModalVisible(true);
                  getEvent();
                }}
              />
            ))}
          </ScrollView>
        </View>
      </SafeAreaView>
    </Fragment>
  );
}
